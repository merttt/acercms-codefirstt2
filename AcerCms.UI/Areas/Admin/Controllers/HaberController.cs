﻿using System;
using System.Data.Entity.Validation;
using System.IO;
using System.Web;
using System.Web.Mvc;
using AcerCms.Dal;
using AcerCms.Dal.Enities;
using AcerCms.UI.Areas.Admin.Models;


namespace AcerCms.UI.Areas.Admin.Controllers
{
    public class HaberController : BaseController
    {
        // GET: Admin/Haber
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult YeniHaber()
        {
            //var news = new News();
            /*
             * Var 
             * Object
             * Dynamic
             * */

            var model = new NewsViewModel()
            {
                isDeleted = true
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult YeniHaber(News news, HttpPostedFileBase file)
        {            
            string fileName = Path.GetFileName(file.FileName);
            file.SaveAs(Server.MapPath("~/Content/Admin/" + fileName));
            try
            {
                using (var db = new ApplicationDbContext())
                {
                    var yeniHaber = new News
                    {
                        Title = news.Title,
                        ShortDescription = news.ShortDescription,
                        Content = news.Content,
                        UpdateDate = DateTime.Today,
                        IsDeleted = news.IsDeleted,
                        ViewCount = news.ViewCount,
                        Picture =  new Picture
                        {
                            
                            Title = news.Title,
                            Description = news.Title,
                            Path = "/Content/Admin/",
                            FileName = fileName                                 
                        }
                    };
                   
                    db.News.Add(yeniHaber);
                    db.SaveChanges();
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Response.Write(string.Format("Entity türü \"{0}\" şu hatalara sahip \"{1}\" Geçerlilik hataları:", eve.Entry.Entity.GetType().Name, eve.Entry.State));
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Response.Write(string.Format("- Özellik: \"{0}\", Hata: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
                    }
                    Response.End();
                }
            }

            return RedirectToAction("YeniHaber");
        }
    }
}