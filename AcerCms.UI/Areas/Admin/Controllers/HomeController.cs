using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AcerCms.UI.Areas.Admin.Controllers
{
    
    [RouteArea("Admin", AreaPrefix = "Panel")]
    [Route("{action=Index}")]
    public class HomeController : BaseController
    {
        // GET: Admin/Home
        public ActionResult Index()
        {
            return View();
        }
    }
}