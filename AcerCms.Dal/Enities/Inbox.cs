using System;

namespace AcerCms.Dal.Enities
{
    public class Inbox
    {
        public int Id { get; set; }
        public string From { get; set; }
        public string FromEmail { get; set; }
        public string Phone { get; set; }
        public string Message { get; set; }
        public bool IsRead { get; set; }
        public DateTime SentDate { get; set; }
    }
}
