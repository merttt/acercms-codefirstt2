using System;
using System.ComponentModel.DataAnnotations;

namespace AcerCms.Dal.Enities
{
    public class Comment
    {
        public int Id { get; set; }

        [MaxLength(100)]
        public string AuthorName { get; set; }
        [MaxLength(400)]
        public string Content { get; set; }
        public DateTime UpdateDate { get; set; }
        public bool IsSpam { get; set; }
        public CommentEntity Entity { get; set; }
        public int EntityId { get; set; }
    }

    public enum CommentEntity
    {
        News,
        Page,
        Picture
    }
}
